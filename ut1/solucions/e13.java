// Programa que llitja una quantitat positiva / negativa de ºC i la passe a graus Farenheit.

public class e13
{
    public static void main(String args[])
    {
        double x,y;

        System.out.println("Introdueix una quantitat de ºC per a passar-los a graus Farenheit.");
        x = Double.parseDouble(System.console().readLine());

        y = x * 1.8 + 32;
        System.out.println(x + " ºC són " + y + (" graus Farenheit"));
    }
}
