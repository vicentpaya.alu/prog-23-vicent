// Exemple de MATRIU de tipus bàsic

public class matriuBasica
{
	public static void main(String args[])
	{
		final int F=2,C=3;	// constants per al nombre de files i columnes
		// primer creem l'array
		double matriu[][] = new double[F][C];
		
		// carreguem valors aleatoris
		for (int i = 0; i < F ; i++)	// i serà l'index de fila
			for (int j = 0; j < C ; j++)		// j serà l'index de columna
				matriu[i][j] = (int) (10*Math.random()) + 1;
				
		// mostrem la matriu per pantalla
		for (int i = 0; i < F ; i++)
		{	// i serà l'index de fila
			for (int j = 0; j < C ; j++)		// j serà l'index de columna
				//System.out.println("Fila " + i + ", columna " + j + " : " + matriu[i][j]);
				System.out.print(matriu[i][j] + "\t");
			System.out.println("");
		}
	}
}
