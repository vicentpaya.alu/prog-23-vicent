// exemple d'array de tipus NO BASIC: array d'objectes

public class arrayRectangles
{
	public static void main(String args[])
	{
		final int NR = 4;
		
		Rectangle rs[] = new Rectangle[NR];
		// el new anterior crea l'array però no els rectangles
		for (int i=0 ; i < NR ; i++)
			rs[i] = new Rectangle(i+1,i+2);
		// mostrar els valors d'ample i alt, així com l'àrea de cada rectangle
		for (int i=0 ; i < NR ; i++)
		{
			System.out.print("Rectangle " + (i+1) + ": ");
			rs[i].mostraRectangle();	// void, fa internament el print
			System.out.println("La seua àrea és " + rs[i].area());	// area és double
		}
	}
}
