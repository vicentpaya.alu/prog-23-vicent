import java.util.Scanner;

public class usefurbolista  
{
	public static void main(String[] args) 
	{
		Scanner ent=new Scanner(System.in);
		
		int nPlayers,goals;
		String name;
		
		System.out.print("Introduce el numero de futbolistas: ");
		nPlayers=ent.nextInt();
		
	
		futbolista fut[]=new futbolista[nPlayers];
		
		//inicializar los objetos
		for(int i=0; i <fut.length ;i++)
		{
			System.out.print("Introduce el nombre del futbolista: ");
			ent.nextLine();	// descarrega l'INTRO o RETURN
			name=ent.nextLine();	// lectura real del nom
			System.out.print("Goles del jugador: ");
			goals=ent.nextInt();
			
			fut[i]=new futbolista(name,goals);
		}
		
		//Ordenar e imprimir los jugadores en orden
		ordenaFutbolistas(fut);
		printPlayers(fut);
		
		System.exit(0);
	}
		
	//Ordenar los jugadores en funcion de los goles
	public static void ordenaFutbolistas(futbolista futbolista[])
	{
		boolean tidy=false;
		int limit=futbolista.length-2,i;
		futbolista aux;
		
		while((limit>=0)&&(tidy==false)) 
		{  
		   i=0;
		   tidy=true;
		   while(i<=limit)
		   {
		   	if(futbolista[i+1].getGoals() > futbolista[i].getGoals()) 
		   	{ 
		   	   aux=futbolista[i];
		   	   futbolista[i]=futbolista[i+1];
		   	   futbolista[i+1]=aux;
		   	}
		   	i++;
		   }
		   limit--;
		}
	}
	
	public static void printPlayers(futbolista fut[])
	{
		for(int i=0;i<fut.length;i++)
		{
			fut[i].printPlayer();
		}
	}
	
	
}
