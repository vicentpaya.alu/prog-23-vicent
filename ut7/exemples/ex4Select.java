/* Ejemplo de SELECT utilizando RESULTSET
 */
import java.sql.*;

public class ex4Select {
    
    public static void main(String[] args) {

        try ( Connection connexio = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/musica","root","root");
        	Statement stmt = connexio.createStatement();
        	ResultSet rs = stmt.executeQuery("select * from discos");	
        ) {
            int cod,nMusico; String titulo; double precio; boolean pos;
            //rs.next();
            pos = rs.isBeforeFirst();
            System.out.println("¿Està al principi (abans del primer)?: " + pos);
            while (rs.next())
            {
            	//cod = rs.getInt(1);
            	cod = rs.getInt("id");
            	//titulo = rs.getString(2);
            	titulo = rs.getString("titol");
            	precio = rs.getDouble("preu");
            	nMusico = rs.getInt("music");
            	System.out.println("Fila: " + rs.getRow() + ", Id: " + cod + ",\t" + titulo + ", preu: " + precio + " euros, del músic " + nMusico);
            }
            //rs.previous();
            pos = rs.isAfterLast();
            System.out.println("¿Està al final (després de l'últim)?: " + pos);
            
        } catch(SQLException se) {
            //Errors de JDBC
            se.printStackTrace();
        } 
    }
    
}
