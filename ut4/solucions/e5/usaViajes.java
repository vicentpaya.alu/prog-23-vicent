import java.util.Scanner;

import viaje.*;

public class usaViajes
{
    public static void main(String[] args)
    {
            coche c1 = new coche(2.3, 40, -4);
            moto m1 = new moto(1.4,150,"Cross");
            vehiculo v1= new moto(2.4,32.2,"Scooter");	// upcasting

            Scanner ent = new Scanner(System.in);

            int km;

            System.out.print("Introduce los Km que se han realizado con el coche: ");
            km=ent.nextInt();
            System.out.println("El importe total es: "+c1.consumViatge(km));

            System.out.print("Introduce los Km que se han realizado con la moto: ");
            km=ent.nextInt();
            System.out.println("El importe total es: "+m1.consumViatge(km));
            System.out.print("Introduce los Km que se han realizado con el vehiculo: ");
            km=ent.nextInt();
            System.out.println("El importe total es: "+v1.consumViatge(km));
            
            System.out.println("El importe integro se realizará por bizum al siguiente numero: 662064042");

           
    }
}
