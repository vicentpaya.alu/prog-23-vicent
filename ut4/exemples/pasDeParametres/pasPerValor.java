// Programa que demane un valor a l'usuari i mostre el seu valor duplicat

/* 
  EXEMPLE EN EL QUE EL PARÀMETRE A LA FUNCIÓ ÉS DE TIPUS BÀSIC: ÉS UN PAS PER VALOR, LA FUNCIÓ DUPLICA TREBALLA
  AMB UNA CÒPIA, LA DADA ORIGINAL NO CANVIA */

import java.util.Scanner;
public class pasPerValor
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		double num;
		
		System.out.println("Introduix un número:");
		num = ent.nextDouble();
		duplica(num);
		System.out.println("El valor duplicat és " + num);

	}
	
	public static void duplica(double n)
	{
		n = 2*n;	// n *= 2
		System.out.println("Dintre de la funció: el valor duplicat és " + n);
	}
}
