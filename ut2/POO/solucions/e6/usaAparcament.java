/*6. En un aparcament volen registrar les entrades de cotxes al llarg de cada dia. Per a això, has de fer un programa que treballe amb una classe Entrada que continga 2 atributs:
	- matrícula del cotxe (String)
	- instant de l'entrada (Rellotge)
Reutilitza la classe Rellotge de l'exercici 1.*/

public class usaAparcament{

    public static void main(String args[]){
        Entrada e1 = new Entrada();	// a les 12 del cotxe particular del parking
        Rellotge r2 = new Rellotge(11,42,30);
        Entrada e2 = new Entrada(r2,"4567LMN");
        Rellotge r3 = new Rellotge(11,46,35);
        Entrada e3 = new Entrada(r3,"9999ZZZ");

        e1.mostraEntrada();
        e2.mostraEntrada();
        e3.mostraEntrada();

        /*Entrada e2 = new Entrada();

        e2.mostraEntrada();*/
        
        // Canviar l'hora d'entrada del 3: 12:46:35 i tornar a mostrar-la
        
        // ...
        
        e3.mostraEntrada();
        
    }
}
