// Segona classe INSTANCIABLE

class Cercle	// al mateix fitxer no poden haber 2 o més classes públiques, només una (la del programa, la que té main)
{
	// atributs o propietats
	private double radi;
	public final static double PI = 3.1416;	// CONSTANT

	// constructor per defecte
	public Cercle() { radi = 1; }
	// constructor general
	public Cercle(double r) { radi = r; }
	// constructor de còpia
	public Cercle(Cercle c) { radi = c.radi; }
	
	// mètodes (funcions internes a la classe)
	public void setRadi (double r)	// SETTER
	{
		// atribut = paràmetre --> NO S'HA DE FER MAI: paràmetre=atribut
		radi = r;
	}
	public double getRadi() { return radi; }
	
	public double perimetre() { return 2 * PI * radi; }
	
	public double area() { return PI * radi * radi; }	// return PI * Math.pow(radi,2);
	
	public void mostraCercle()
	{
		System.out.println("Radi: " + radi);
	}
}

// Programa que utilitza la classe instanciàble anterior
public class usaCercle
{
	public static void main(String[] args) {
		Cercle c1 = new Cercle();	// constructor per defecte amb radi 1
		Cercle c2 = new Cercle(5);	// constructor general
		Cercle c3 = new Cercle(c1);	// constructor de còpia
		
		c1.mostraCercle();
		c2.mostraCercle();
		c3.mostraCercle();
		
		// mostrar el valor de PI en cercle
	
		//System.out.println(c3.PI);
		// Quan un objecte és estàtic es sol cridar amb el nom de la classe
		System.out.println(Cercle.PI);
		// PI ja existia en la classe Math
		System.out.println(Math.PI);
		
		
		
		System.exit(0);
	}
}
