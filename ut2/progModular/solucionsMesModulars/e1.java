//1. Realitzar una funció que reba una variable de tipus caràcter i si és una lletra minúscula la retorne convertida en majúscules.

public class e1
{
    public static void main (String args[])
    {
       char lt;
       System.out.println("Introduce una letra:");
       lt=System.console().readLine().charAt(0);
       System.out.println("La letra es: "+ minamay(lt));
       System.exit(0);
    }
    
    public static char minamay(char let){
        //restar 32 para convertir a a mayusculas, sumar 32 para convertir de mayusculas a minusculas.        
        if (let>=97 && let<=122)	// si és minúscula
            let=(char)(let-32);
        
       return let;
        }
}

