// solució RECURSIVA del factorial

public class factorialRecursiu
{
	public static void main(String[] args)
	{
		int num;
		
		do
		{
			System.out.println("Introduix un enter :");
			num = Integer.parseInt(System.console().readLine());
		} while ( num < 1);
		// ús de la funció
		System.out.println("El factorial és " + factorial(num)); // crida a la funció factorial des de l'interior del println (només vàlid per a funcions no void

		System.exit(0);
	}
	
	// definició RECURSIVA
	public static long factorial(int n)
	{
		if (n < 2 )
			return 1;
		else
			return n*factorial(n-1);
	}
}
