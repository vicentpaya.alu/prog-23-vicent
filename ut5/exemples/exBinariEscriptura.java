/* Programa que escriga 100 números de loteria, aleatoris , a un fitxer binari */

import java.io.*;

class exBinariEscriptura
{
	public static void main(String[] args) {
			final int LIM = 100;
			final int TAM = 100000;
			try{
				FileOutputStream fos = new FileOutputStream("nums.dat");
				DataOutputStream dos = new DataOutputStream(fos);
				for (int i=0 ; i < LIM ; i++)
				{
					int num = (int) (TAM*Math.random());
					System.out.println(num);
					// escric a fitxer el número
					dos.writeInt(num);
				}
				dos.close();
				fos.close();
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
	}
}
