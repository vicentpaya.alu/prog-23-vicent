(defn saluda
"Mostra una salutació als noms que rep como a paràmetres"
([] (println "Hola món"))
([nom] (println "Hola, " nom))
([nom & rest] (println "Hola, " nom " y "
(clojure.string/join " i " rest))))

(saluda)
(saluda "pepe")
(saluda "uno" "dos" "tres" "cuatro")
